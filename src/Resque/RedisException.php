<?php

namespace Resque;

/**
 * Redis related exceptions
 *
 * @package        Resque
 * @author         Daniel Mason <daniel@m2.nz>
 * @license        http://www.opensource.org/licenses/mit-license.php
 */

class RedisException extends \Exception
{
}
